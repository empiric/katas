Intenta ahora ver el contenido del archivo **pom.xml** usando `cat pom.xml`{{execute}}.

Deberías observar que tiene las dependencias tal como se explicó en la guía:

![pom](https://gitlab.com/empiric/katas/raw/master/ic-maven/assets/i3.png)

Ahora compilemos el proyecto:

`mvn compile`{{execute}}

En este punto podrás observar cómo se descargan las dependencias necesarias. Al final notarás un mensaje que indica que el proyecto se construyó correctamente.

![compilación](https://gitlab.com/empiric/katas/raw/master/ic-maven/assets/i4.png)

Al finalizar, se crea el directorio **target** con los archivos ya compilados.

![compilación](https://gitlab.com/empiric/katas/raw/master/ic-maven/assets/i2.png)

Finalmente, puedes ejecutar las pruebas:

`mvn test`{{execute}}

Puedes observar ahora que se ejecutaron correctamente los tres tests definidos.

![Pruebas](https://gitlab.com/empiric/katas/raw/master/ic-maven/assets/img2.png)

