El primer paso será ejecutar Jenkins como un contenedor Docker. Las instrucciones para iniciarlo se encuentran a continuación:

`docker run -d --name jenkins \
    -p 8080:8080 -p 50000:50000 \
    registry.gitlab.com/empiric/ic/jenkins-host:v2`{{execute}}

Solo con pulsar sobre el comando este será copiado a la terminal y ejecutado. El contenedor está basado en una imagen de Linux Alpine para reducir el tamaño de la descarga. Aún así, pueden pasar algunos minutos mientras el contenedor es descargado y ejecutado. Con el parámetro _-p_ se indica que la interfaz de administración Web de Jenkins será accesible en el puerto 8080, mientras que el puerto 50000 se utiliza para comunicarse con otros agentes de Jenkins.

En el momento que se inicie Jenkins, podrá ver el estado usando: `docker ps`{{execute}}.

#### Administración de Jenkins

Para acceder a la interfaz de administración de Jenkins, en el panel de la derecha seleccione la pestaña _Jenkins_ que se encuentra al lado de _Terminal_. Para abrir la pestaña también puede utilizar el enlace: https://[[HOST_SUBDOMAIN]]-8080-[[KATACODA_HOST]].environments.katacoda.com/

Puede abrir el enlace en una nueva ventana si desea poder ver la aplicación maximizada. 

En los siguientes pasos vamos a configurar el plugin de Docker y a comenzar a construir imágenes.
