Dentro de unos minutos podrás acceder a Jenkins usando <a href="https://[[HOST_SUBDOMAIN]]-80-[[KATACODA_HOST]].environments.katacoda.com/" target="_">esta dirección</a> (se recomienda abrir en una nueva ventana).

Ahora puede seguir las instrucciones indicadas en la guía, en la sección llamada _Laboratorio_.
