Bienvenido a este reto de seguridad Web. Puedes acceder al sitio abriendo el siguiente enlace en una nueva ventana:
https://[[HOST_SUBDOMAIN]]-80-[[KATACODA_HOST]].environments.katacoda.com/

Inicialmente la aplicación se está instalando a sí misma. Pueden pasar algunos minutos mientras se completa este proceso. Una vez finalizado, la página se actualizará y observarás un formulario de login, donde podrás autenticarte con las credenciales: _udea_ / _udea_. El reto consiste en descubrir la clave del usuario _admin_. ¡Mucha suerte!

NOTA: Cada vez que se recarga esta página, se desplegará una nueva instalación de la aplicación en una dirección diferente que estará disponible durante una hora.

`file.txt`{{open}}

`comando`{{copy}}
