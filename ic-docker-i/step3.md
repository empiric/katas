Ahora configuraremos el plugin de Docker para utilizar el servicio de Docker.

1. Seleccione **Manage Jenkins** y luego **Configure System** para acceder a la configuración principal de Jenkins.
2. En la parte inferior, en la sección **Cloud** existe una elemento con el nombre **Docker**.
3. Ahora puede configurar las opciones del contenedor. El campo _Name_ indica el nombre del agente. Para este ejemplo usaremos **agente-docker**.
4. En el campo _Docker URL_ es donde se indica a Jenkins en cuál servicio Docker iniciar el agente. En este caso, usaremos el mismo servicio que ejecuta al propio Jenkins (para ambientes de producción es mejor utilizar máquinas diferentes para mayor escalabilidad). Escriba la URL **[tcp://[[HOST_IP]]:2345](tcp://[[HOST_IP]]:2345)**
5. Use **Test Connection** para verificar que Jenkins pueda comunicarse con el servicio de Docker. Como resultado debe ver el número de versión de Docker devuelto.
6. Haga clic en **Save**.

Este ambiente virtual ya cuenta con la configuración de la imagen para el agente. Para consultar los pasos de cómo se realizó, lea la guía que acompaña este laboratorio. Jenkins ahora puede iniciar un agente en un contenedor Docker cuando sea necesario durante el Build.
