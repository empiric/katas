El primer paso para configurar Jenkins es instalar el [Plugin de Docker](https://wiki.jenkins-ci.org/display/JENKINS/Docker+Plugin), que está basado en el plugin de Cloud para Jenkins. Esto significa que lo que aprenderá sobre cómo configurar un agente Docker en este laboratorio, le será útil cuando quiera configurar agentes que se ejecuten en la nube, como por ejemplo GCP o AWS.

Cuando el build sea creado, utilizará el _agente de tipo Cloud_ para ejecutar todo el Build. El agente será un contendor Docker configurado para comunicarse con el servicio principal de Docker. Este servicio Docker también se conoce como el demonio Docker (Docker Daemon).

Jenkins creará la imagen y la almacenará en el servicio de Docker configurado. La imagen puede luego ser enviada a un registro de Docker encargado del despliegue.

#### Instrucciones: Instalar el plugin

1. En el portal de administración, seleccione **Manage Jenkins** a la izquierda.
2. En la página de configuración, seleccione **Manage Plugins**.
3. En esa página podrá ver los complementos actualmente instalados, como por ejemplo el **Git plugin** que perimte obtener el código fuente de un repositorio Git.
4. Seleccione la pestaña de **Available** para ver todos los complementos que se pueden instalar.
5. Ingrese **Docker plugin** en el cuadro de búsqueda. Hay varios complementos de Docker, necesitará el que se llama **Docker plugin**. Selecciónelos utilizando las casillas de verificación.
6. Haga clic en **Install without Restart** en la parte inferior.
![Instalar plugin Docker](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/plugin-install.png)

El plugin ahora será descargado e instalado. Una vez completado el proceso, haga clic en el enlace **Go back to the top page**.

Ahora el servidor Jenkins esta listo para ser configurado para construir imágenes Docker.
