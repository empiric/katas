En este escenario, aprenderá a configurar Jenkins integrado con Gitlab y Selenium. 

El escenario está diseñado para demostrar cómo se pueden utilizar diferentes herramientas dentro de un Pipeline de integración continua, utilizando las imágenes como un artefacto de construcción que se puede promover a diferentes entornos y finalmente la producción. Los pasos le guían para instalar el plugin, crear una compilación y ver los resultados.
