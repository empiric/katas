Ahora tenemos un Build configurado que construirá imagenes Docker basadas en el Dockerfile especificado en el repositorio de Git. La siguiente etapa es ejecutar el Build.

#### Instrucciones: Ejecutar el Build

Dentro del proyecto, en el lado izquierdo, seleccione **Build Now**. Otra opción es mediante la pequeña pestaña al lado del nombre del proyecto:

![Build Now](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/build-now.png)

Debería ver un Build programado con un mensaje que indica que se está en cola esperando que el siguiente ejecutor esté disponible:

![Build queue](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/build-queue.png)

En background Jenkins está lanzando el contenedor y conectándose a él a través de SSH. A veces esto puede tomar unos minutos.

![Agente en ejecución](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/running-agent.png)

También puede ver el progreso usando en la terminal: `docker logs --tail=10 jenkins`{{execute}}

Como es un contenedor, puedes explorarlo usando las herramientas de la CLI de Docker como:
`docker ps -a`{{execute}}

Puedes utilizar `docker info`{{execute}} para ver la información del servicio Docker, incluyendo la cantidad de imágenes.

Desde la interfaz gráfica de Jenkins puede consultar la salida por consola usando el pequeño círculo al lado del número de Build. El color azul indica que se ha completado la construcción satisfactoriamente.

![Ir a la salida por consola](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/go-console.png)

En la salida por consola también puede confirmar que el Build ha finalizado correctamente cuando vea el mensaje _Finished: SUCCESS_ al final.

![Resultado del Build](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/result.png)
