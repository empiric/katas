Primero ingresa al directorio que contiene la aplicación:

`cd calculadora`{{execute}}

El comando de Linux `ls`{{execute}} nos permite ver los archivos y directorios en la ruta actual. Observa que no existe el directorio **target**, sino únicamente el directorio **src** que es donde se almacenan los archivos de código fuente. También está el archivo **pom.xml** que es donde se almacena la configuración del proyecto.

![ls](https://gitlab.com/empiric/katas/raw/master/ic-maven/assets/i1.png)

Puedes utilizar el comando `ls -R`{{execute}} para ver todos los directorios y archivos del proyecto. Por ejemplo en **src/test** es donde se encuentran la clases con el código de las pruebas. Observa que aún no hay archivos compilados **.class**.

Para ver el contenido de un archivo específico puedes usar el comando _cat_ seguido de la ruta:
`cat src/test/java/calculadora/CalculadoraTest.java`{{execute}}

