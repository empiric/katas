Puedes ejecutar la aplicación como un contenedor Docker. Las instrucciones para iniciarlo se encuentran a continuación:

`docker run -d -p 8080:8080 -p 8090:8090 empiric/vulnf:v1 zap-webswing.sh`{{execute}}

Solo con pulsar sobre el comando este será copiado a la terminal y ejecutado. Pueden pasar algunos minutos mientras el contenedor es descargado y ejecutado.

Puedes acceder al sitio usando la pestaña _Aplicación_ al lado derecho o abriendo el siguiente enlace en una nueva ventana:
https://[[HOST_SUBDOMAIN]]-8080-[[KATACODA_HOST]].environments.katacoda.com/?anonym=true&app=ZAP
