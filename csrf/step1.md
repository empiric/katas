Puedes ejecutar la aplicación como un contenedor Docker. Las instrucciones para iniciarlo se encuentran a continuación:

`docker run --name web -d -p 80:80 -p 3306:3306 -e MYSQL_PASS="11111111" registry.gitlab.com/empiric/public-dvwa-csrf/lab:v3`{{execute}}

Solo con pulsar sobre el comando este será copiado a la terminal y ejecutado. Pueden pasar algunos minutos mientras el contenedor es descargado y ejecutado.

Puedes acceder al sitio usando la pestaña _Aplicación_ al lado derecho o abriendo el siguiente enlace en una nueva ventana:
https://[[HOST_SUBDOMAIN]]-80-[[KATACODA_HOST]].environments.katacoda.com/

Deberás iniciar la base de datos pulsando en el botón **Create / Reset Database** y luego podrás acceder al login. Credenciales:
**udea** / **udea**.
