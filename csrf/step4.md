Este paso crea un nuevo proyecto que Jenkins construirá a través de nuestro nuevo agente. El código fuente del proyecto se encuentra en https://github.com/katacoda/katacoda-jenkins-demo. El repositorio tiene un Dockerfile donde se definen las instrucciones sobre cómo producir la imagen de Docker.

#### Instrucciones: Crear proyecto tipo Pipeline 

1. Seleccione **New Item**
2. Asigne al build un nombre como por ejemplo **proyecto-docker**, seleccione como tipo de proyecto **Pipeline** y pulse en **OK**.
3. Copie el siguiente contenido en el área de texto de la sección _Pipeline > Definition > Script_.
`node("agente-docker") {
  withDockerServer([uri: "tcp://..."]) { //Reemplazar valor en uri
    withDockerRegistry([credentialsId: 'clave-agente', url: "https://hub.docker.com/r/benhall/dind-jenkins-agent/"]) {
      git url: "https://github.com/katacoda/katacoda-jenkins-demo"
      def image = docker.build("katacoda/jenkins-demo:${BUILD_NUMBER}")
    }
  }
}`
4. El Pipeline se empieza a definir usando `node("agente-docker")`, que indica que el Build solo podrá ser ejecutado en agentes con el label _agente-docker_. Esto es necesario, porque el Build depende de poder ejecutar comandos Docker, así que el nodo seleccionado deberá estar en la capacidad de ejecutarlos. El agente que creamos en el paso anterior, al que le pusimos la etiqueta _agente-docker_, precisamente cuenta con una instalación de cliente Docker. 
5. En el campo _uri_ de _withDockerServer_, reemplace el valor por **[tcp://[[HOST_IP]]:2345](tcp://[[HOST_IP]]:2345)**. Esto le permitirá al agente enviar instrucciones al servicio Docker.
6. Toda la lógica de cómo construir la imagen se especifica en el Dockerfile almacenado en el código fuente de nuestro repositorio, por ello Jenkins no necesita indicar el nombre de la imágen resultante.
7. Usamos _withDockerRegistry_ para indicar cuál es el registro Docker que tiene la imagen del agente, así como el identificador de las credenciales que previamente creamos para poder hacer la autenticación con el agente usando SSH.
8. Con el comando `git` se está indicando la URL del repositorio de código que será clonado y sobre el cuál se realizarán las tareas del Build. Este repositorio es el que cuenta con el Dockerfile que describe cómo construir la imagen.
9. Se ejecuta `docker.build` para construir la imagen, que será llamada como lo indica el argumento, usando además la variable __BUILD_NUMBER__ de Jenkins para asociar el número del Build como etiqueta para la imagen. Esto permite luego identificar cuál Build fue el responsable de generar cada una de las imágenes. El equivalente de la instrucción `docker.build`, si se usara la interfaz de línea de comandos (CLI) de Docker, sería: `docker build -t katacoda/jenkins-demo:${BUILD_NUMBER} -f ./Dockerfile` La ventaja de no utilizar la CLI, sino el plugin de Docker para Jenkins, es que esa instrucción será ejecutada automáticamente en el pipeline de integración continua. También opcionalmente puede ejecutar el comando `docker tag katacoda/jenkins-demo:${BUILD_NUMBER} katacoda/jenkins-demo:latest` para indicar que la versión más reciente de nuestra imagen es la que acabamos de construir. También opcionalmente podría ejecutar `docker push` para enviar la nueva versión de la imagen al repositorio (esto solo funcionará si se tienen los permisos adecuados sobre el registro Docker configurado).
10. Ahora que ha completado la configuración del proyecto, puse en **Save**.
