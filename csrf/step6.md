Una vez que la construcción del Build haya terminado, debería poder ver las imágenes y las etiquetas utilizando en la terminal: `docker images`{{execute}}.

Lo que se construyó en la imagen Docker era un pequeño servidor HTTP. Para iniciarlo puede utilizar:
`docker run -d -p 80:80 katacoda/jenkins-demo:1`{{execute}}

Usando cURL puede ver que el servidor responde:
`curl docker`{{execute}}

Jenkins tendrá disponible la salida de consola de nuestro Build a través del panel de administración. Debería poder acceder a él usando el enlace a continuación:

https://[[HOST_SUBDOMAIN]]-8080-[[KATACODA_HOST]].environments.katacoda.com/job/proyecto-docker/lastSuccessfulBuild/console

Observe que el enlace asume que el Build tiene como nombre _proyecto-docker_.
