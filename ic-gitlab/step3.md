Una vez que los complementos se han instalado, puede configurar cómo se lanzan los contenedores Docker. La configuración le indicará al complemento cuál imagen Docker utilizar para el agente y cuál sevicio (demonio) de Docker ejecutará los contenedores.

El complemento trata a Docker como un proveedor de la nube, iniciando contenedores cuando la creación del Build lo requiera.

#### Instrucciones: Configurar el complemento

En este paso se configura el complemento para comunicarse con el servicio Docker que se encuentra en el host.

1. Seleccione de nuevo **Manage Jenkins**, pero esta vez ingrese a la opción **Configure System** para acceder a la configuración principal de Jenkins.
2. En la parte inferior, hay una lista desplegable llamada **Add a new cloud**. Dentro de esa lista seleccione **Docker**.
3. Ahora puede configurar las opciones del contenedor. El campo _Name_ indica el nombre del agente. Para este ejemplo usaremos **agente-docker**.
4. En el campo _Docker URL_ es donde se indica a Jenkins en cuál servicio Docker iniciar el agente. En este caso, usaremos el mismo servicio que ejecuta al propio Jenkins (para ambientes de producción es mejor utilizar máquinas diferentes para mayor escalabilidad). Escriba la URL **[tcp://[[HOST_IP]]:2345](tcp://[[HOST_IP]]:2345)**
5. Use **Test Connection** para verificar que Jenkins pueda comunicarse con el servicio de Docker. Como resultado debe ver el número de versión de Docker devuelto.

#### Instrucciones: Configurar la imagen

Nuestro plugin ahora puede comunicarse con Docker. Ahora configuraremos cómo iniciar el contenedor a partir de la imagen de Docker para el agente.

1. Utilizando la lista desplegable en el campo _Images_, seleccione **Add Docker Template** y luego **Docker Template**.
2. Para el campo _Docker Image_, utilice **benhall/dind-jenkins-agent**. Esta imagen está configurada con un cliente Docker.
3. Para especificar que el Build utilice el agente Docker, asigne el _label_ (etiqueta) con el mismo valor configurado antes para el campo _Name_ del agente (**agente-docker**).
4. Jenkins usa SSH para comunicarse con los agentes. Agregue un nuevo conjunto de credenciales mediante la opción **Add** de la sección _Credentials_. El nombre de usuario del contenedor es **jenkins** y la contraseña es **jenkins**. En el campo _ID_ utilice **clave-agente**, opcionalmente agregue una descripción y pulse en **Add**.
![Formulario de credenciales](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/creds1.png)

Asegúrese de cambiar luego la lista desplegable para seleccionar las credenciales recien creadas (en vez del -none- que aparece).
![Formulario de credenciales](https://gitlab.com/empiric/katas/raw/master/ic-docker/assets/creds2.png)

5. Finalmente, expanda la sección _Container Settings_ que se encuentra al principio de la sección _Docker Template_, justo abajo del campo _Docker Image_. En el cuadro de texto _Volumes_, escriba **/var/run/docker.sock:/var/run/docker.sock**.
6. Haga clic en **Save**.

Jenkins ahora puede iniciar un agente en un contenedor Docker cuando sea necesario durante el Build.
